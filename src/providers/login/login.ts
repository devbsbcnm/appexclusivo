import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class LoginProvider {

	constructor(
		public http: Http
	) {
		
	}

	public login(email,password) {
        try { 
            let body = JSON.stringify({
                login: email,
                senha: password,
                pagina: ""
            });

            return this.http.post('http://172.16.10.117:8000/exclusivo/logar/index', body)
                .toPromise()
                .then(response => {                	
                		console.log(response);
                        return response.json();
                    },
                    e => {
                        console.log(e)
                    });
        } catch(e) {
            console.log(e);
		}
	}
}
