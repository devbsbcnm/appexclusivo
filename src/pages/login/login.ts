import {Component,ViewChild} from '@angular/core';
import { NavController , LoadingController, AlertController } from 'ionic-angular';
import {ForgotPasswordPage} from '../forgot-password/forgot-password';
import {SignUpPage} from '../sign-up/sign-up';
import {MainTabsPage} from '../main-tabs/main-tabs';
import {LoginProvider} from '../../providers/login/login';
/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    @ViewChild('username') username;
    @ViewChild('password') password;

    public debugger: string;

    constructor(
        public nav: NavController,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public loginUser: LoginProvider
    ) {

    }

    // go to forgot password page
    forgotPwd() {
        this.nav.push(ForgotPasswordPage);
    }

    private alert(title, msg) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['OK']
        });
        alert.present();
    }

    // process login
    login() {
        // localStorage.clear();
        try {
            if (this.username.value == "" || this.password.value == "") {
                this.alert("Preencha os campos", "Para conseguir efetuar o login e necessário que preencha todos os campos!");
                return false;
            } else {
                var loader = this.loadingCtrl.create({
                    content: "Aguarde por favor..."
                });
                loader.present();
                this.loginUser.login(this.username.value, this.password.value).then((t) => {
                    loader.dismiss();
                    if(t.error) {
                        this.alert("Erro!",t.error);
                        return false;
                    }
                    localStorage.setItem('user',t.success);

                    window.location.reload();
                });
            }
        } catch (e) {
            console.log(e);
            loader.dismiss();
        }
        // add your login code here
        // this.nav.setRoot(MainTabsPage);
    }

    // go to sign up page
    signUp() {
        // add our sign up code here
        this.nav.push(SignUpPage);
    }

    public maskCPF(event) {
        if(event.keyCode != 8) { 
            let e = (<HTMLInputElement>event.target);
            if(e.value.length == 3)
                e.value = e.value + '.';
            if(e.value.length == 7)
                e.value = e.value + '.';
            if(e.value.length == 11)
                e.value = e.value + '-';
            if(e.value.length >= 14)
                return false;
        }    
    }
}