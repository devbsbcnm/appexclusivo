import {Component,ViewChild} from '@angular/core';
import {Platform,NavController,LoadingController} from 'ionic-angular';
import {StatusBar} from 'ionic-native';

// import pages
import {LoginPage} from '../pages/login/login';
import {AccountPage} from '../pages/account/account';
import {MainTabsPage} from '../pages/main-tabs/main-tabs';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {

    public rootPage: any;

    @ViewChild('content') nav

    constructor(
        public loadingCtrl: LoadingController,
        public platform: Platform
    ) {
        let l = localStorage.getItem('user');
        if(l == null)
            this.rootPage = LoginPage;
        else 
            this.rootPage = MainTabsPage;

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            StatusBar.styleDefault();
        });
    }

    public account() {
        this.nav.push(AccountPage);
    }

    public logout() {
        var loader = this.loadingCtrl.create({
            content: "Aguarde por favor..."
        });
        loader.present();
        localStorage.clear();
        setTimeout(() => {
            window.location.reload();
        },600);
    }

}